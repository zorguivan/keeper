Done - The install file is not complete, scripts to create and select database are missing.
Done - Masked input from times.
Done- Dates have to be converted to UNIX timestamp, and only a valid date can be selected, the application have to give an error if the date is not valid.
Done - The project have to setup correctly, remove the git and reinstall, to see how to fix this issue.
Done- The date input should have a default value of the current date.
Done- The start time input should have a default value of the current time.
Done- In the end time input it should add the whole record when enter is pressed.
Done- When the record is added without an end time. Display in the table in the column of hours, that the session is not ended.  The user should be able to enter the endtime, and press enter to confirm.
Done - The user should be able to delete sessions. But a confirmation has the be shown with the following message. "Are you sure you want to delete the session on {date} from {starttime} to {endtime}".
- I want to be able to add day notes. A text area have to be shown, and when I finish typing, it should trigger an update.
The system automatically has to reset the input, every day (00:00). And add the previous notes to a table.
  Where it does not display the note itself, but when the user presses open,
   then the note should be shown in a dialog.
    (Notes from the previous days are not allowed to be edited.)
