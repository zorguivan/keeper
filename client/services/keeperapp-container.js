class KeeperAppContainer{
    constructor(){
        this.modules = [];
        this.navigation = [];
    }

    registerModule(module){
      console.log("Saving", module);
        this.modules.push(module);
    }

    getModules(){
        return this.modules;
    }

    getNavigation(){
        return this.navigation;
    }

    registerNavigation(navigationItem){
        this.navigation.push(navigationItem);
    }

}

export default new KeeperAppContainer();
