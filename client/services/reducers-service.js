class ReducersService{
    constructor(){
        this.reducers = {};
    }

    registerReducer(name, reducer){
        this.reducers[name] = reducer;
    }

    reduce(state, action){
        console.log("Triggering " + action.type);
        if (this.reducers[action.type]){
            return this.reducers[action.type](state, action);
        }
        else{
            return state || {};
        }
    }
}

export default new ReducersService();
