import {createStore} from 'redux';
import ReducersService from './services/reducers-service';

class KeeperStore{
    constructor(){
        this.store = createStore(ReducersService.reduce.bind(ReducersService));
    }

    dispatch(event){
      // console.log('Dispatching ',event)
        this.store.dispatch(event);
    }

    subscribe(func){
        return this.store.subscribe(this.stateChange.bind(this, func));
    }

    stateChange(func){
      // console.log('Replying to dispatch order', this.store.getState());
        let state = this.store.getState();
        func(state);
    }

    getState(){
        return this.store.getState();
    }
}

export default new KeeperStore();
