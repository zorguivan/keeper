import KeeperAppContainer from './services/keeperapp-container';

import tracker from './tracker/tracker-module';

import KeeperStore from './keeper-store';

KeeperAppContainer.registerModule('ngRoute');
KeeperAppContainer.registerModule('ui.bootstrap');
KeeperAppContainer.registerModule('toastr');
KeeperAppContainer.registerModule('tracker');


angular.module('keeper', KeeperAppContainer.getModules())
    .constant('KeeperAppContainer', KeeperAppContainer)
    .constant('Store', KeeperStore)
    .config(mainConfig);


mainConfig['$inject'] = ['$routeProvider'];

function mainConfig($routeProvider) {
  console.log('loading Default Location');
    $routeProvider.otherwise({
        templateUrl: 'views/dashboard.html'
    })
}

function bootstrap(){
    console.log("Bootstrapping");
    angular.element(document).ready(function() {
        angular.bootstrap(document, ['keeper']);
    });
}

$(document).ready(function(){
  bootstrap();
});
