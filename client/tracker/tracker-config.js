export default ['$routeProvider', function($routeProvider){
  $routeProvider.when('/project/:id', {
    templateUrl: './tracker/views/project.html',
    controller: 'ProjectsController',
    controllerAs: 'pc'
  });
  $routeProvider.when('/notes/:id', {
    templateUrl: './tracker/views/notes.html',
    controller: 'NotesController',
    controllerAs: 'nc'
  });
}]
