import Store from '../../keeper-store';

export default class SessionsService {
    constructor($http, toastr) {
        this.$http = $http;
        this.toastr = toastr;
        this.init();
    }

    init() {
        console.log('Sessions Service has been called');
    }

    addSession(session) {
        return this.$http.post('/api/sessions', session).then((res) => {
            this.toastr.info('Session Added');
        }).catch((error) => {
            console.log(error);
            Store.dispatch({
                type: "SERVER_ERROR",
                error: error
            });
            this.toastr.error('Error Starting the Session, Please try again later.');
            return error;
        });
    }

    updateSession(session) {
      console.log('Update request', session);
        return this.$http.put('/api/sessions', session).then((res) => {
            this.toastr.success('Session has been ended.');
            this.getSessions();
        }).catch((error) => {
            Store.dispatch({
                type: "SERVER_ERROR",
                error: error
            });
            this.toastr.info('Faild to end Session, Please try again later');
        });
    }

    deleteSession(sessionId){
      return this.$http.delete('/api/sessions/' + sessionId).then((res)=> {
        this.toastr.success('Session has been deleted');
        this.getSessions();
      }).catch((error) => {
        Store.dispatch({
          type: "SERVER_ERROR",
          error: error
        });
        console.log(error);
      });
      this.toastr.error('Failed to delete the session, Please try again later');
    }

    getProjectSessions(projectId) {
        return this.$http.get('/api/sessions/' + projectId).then((res) => {
            this.toastr.info('Loading the last Session.. !');
            Store.dispatch({
                type: "GET_SESSION",
                session: res.data
            });
            return res.data
        }).catch((error) => {
            Store.dispatch({
                type: "SERVER_ERROR",
                error: error
            });
            this.toastr.info('There is no active Session to load.');
        });
    }
    getSessions() {
        return this.$http.get('/api/sessions').then((res) => {
            this.toastr.info('Sessions Loaded');
            Store.dispatch({
                type: "GET_SESSIONS",
                sessions: res.data
            });
            return res.data;
        }).catch((error) => {
            Store.dispatch({
                type: "SERVER_ERROR",
                error: error
            });
            this.toastr.info('There is no Sessions to load.');
        });
    }


    deleteSession(id) {
      console.log(id);
        return this.$http.delete('/api/sessions/' + id).then(() => {
            this.toastr.success('The Session has been deleted!');
            this.getSessions();
        }).catch((error) => {
            console.log(error);
            Store.dispatch({
                type: "SERVER_ERROR",
                error: error
            });
            this.toastr.error('Error deleting the session. Please try again later.');
            return error;
        });
    }

}
