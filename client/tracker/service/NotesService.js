import Store from '../../keeper-store';

export default class NotesService {
  constructor($http, toastr){
    this.$http = $http;
    this.toastr = toastr;
  }

  getNotes(projectId){
    console.log('Getting Project Notes', projectId);
    return this.$http.get('/api/p_notes/' + projectId).then((res) => {
      console.log(res);
      Store.dispatch({
        type: 'GET_NOTES',
        notes: res.data
      });
      return res.data;
    }).catch((error) => {
      this.toastr.error('Error in loading notes please try again later');
      Store.dispatch({
        type: 'SERVER_ERROR',
        error: error
      });
      return error;
    });
  }

  getNote(noteDate) {
    console.log(noteDate);
    return this.$http.get('/api/notes/' + noteDate).then((res) => {
      Store.dispatch({
        type: 'GET_NOTE',
        note: res.data
      });
      console.log(res);
      return res.data[0];
    }).catch((error) => {
      Store.dispatch({
        type: 'SERVER_ERROR',
        error: error
      });
      return error;
    });
  }

  addNote(note){
    return this.$http.post('/api/notes', note).then((res) => {
      this.toastr.success('The Note Has Been Added!');
      this.getNotes();
    }).catch((error) => {
      Store.dispatch({
        type: 'SERVER_ERROR',
        error: error
      });
      this.toastr.error('Error Adding a Note, Please try again later.')
      return error;
    });
  }

  updateNote(note){
    return this.$http.put('/api/notes', note).then((res) => {
    }).catch((error) => {
      Store.dispatch({
        type: "SERVER_ERROR",
        error: error
      });
      this.toastr.error('Error Editing the Note, Please try again later');
      return error;
    })
  }


}
