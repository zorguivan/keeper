import Store from '../../keeper-store';

export default class ProjectsService {
  constructor($http, toastr){
    this.$http = $http;
    this.toastr = toastr;
  }

  getProjects() {
    return this.$http.get('/api/projects').then((res) => {
      Store.dispatch({
        type: 'GET_PROJECTS',
        projects: res.data
      });
      return res.data;
    }).catch((error) => {
      this.toastr.error('Error in loading projects please try again later');
      Store.dispatch({
        type: 'SERVER_ERROR',
        error: response
      });
      return error;
    });
  }

  getProject(projectId) {
    return this.$http.get('/api/projects/' + projectId).then((res) => {
      Store.dispatch({
        type: 'GET_PROJECT',
        project: res.data[0]
      });
      return res.data[0];
    }).catch((error) => {
      Store.dispatch({
        type: 'SERVER_ERROR',
        error: error
      });
      return error;
    });
  }

  addProject(project){
    return this.$http.post('/api/projects', project).then((res) => {
      console.log(res);
      this.toastr.success('The Project Has Been Added!');
      this.getProjects();
    }).catch((error) => {
      Store.dispatch({
        type: 'SERVER_ERROR',
        error: error
      });
      this.toastr.error('Error Adding a Project, Please try again later.')
      return error;
    });
  }

  updateProject(project){
    return this.$http.put('/api/projects', project).then((res) => {
      console.log(res);
      this.toastr.success("The Name of the Project has been edited!");
      this.getProject(project.id);
    }).catch((error) => {
      Store.dispatch({
        type: "SERVER_ERROR",
        error: error
      });
      this.toastr.error('Error Editing the Name of the Project, Please try again later');
      return error;
    })
  }

  deleteProject(id){
    console.log('Service Deleting project .');
    return this.$http.delete('/api/projects/' + id).then((res) => {
      this.toastr.success('The Project has been deleted!');
      this.getProjects();
    }).catch((error) => {
      console.log(error);
      Store.dispatch({
        type: "SERVER_ERROR",
        error: error
      });
      this.toastr.error('Error deleting the project. Please try again later.');
      return error;
    });
  }

}
