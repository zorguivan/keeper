export default function(){
    return {
      restrict: 'A',
      link: (scope, element, attributes) => {
        $(element).mask(element[0].dataset.mask, {
          placeholder: element[0].dataset.placeholder
        });
      }
    }
}
