import ReducersService from '../../services/reducers-service';

ReducersService.registerReducer('GET_PROJECTS', (state, action) => {
  state = Object.assign({}, state, {projects : action.projects});
  return state;
});

ReducersService.registerReducer('GET_PROJECT', (state, action) => {
  state = Object.assign({}, state, {project : action.project});
  return state;
});

ReducersService.registerReducer('SERVER_ERROR', (state, action) => {
  console.log(action.error);
  return state;
});
