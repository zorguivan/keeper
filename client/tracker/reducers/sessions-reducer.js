import ReducersService from '../../services/reducers-service';

ReducersService.registerReducer('SERVER_ERROR', (state, action) => {
  console.log(action.error);
  return state;
});

ReducersService.registerReducer('GET_SESSIONS', (state, action) => {
  state = Object.assign({}, state, {sessions: action.sessions});
  return state;
})
