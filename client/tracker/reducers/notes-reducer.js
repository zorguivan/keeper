import ReducersService from '../../services/reducers-service';

ReducersService.registerReducer('GET_NOTES', (state, action) => {
  state = Object.assign({}, state, {notes : action.notes});
  return state;
});

ReducersService.registerReducer('GET_NOTE', (state, action) => {
  state = Object.assign({}, state, {note : action.note});
  return state;
});

ReducersService.registerReducer('SERVER_ERROR', (state, action) => {
  console.log(action.error);
  return state;
});
