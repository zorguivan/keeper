export default class ProjectsController {
    constructor(projectsService, sessionsService, notesService, $routeParams, $uibModal, $scope, toastr, store) {
        this.projectsService = projectsService;
        this.sessionsService = sessionsService;
        this.notesService = notesService;
        this.$routeParams = $routeParams;
        this.$scope = $scope;
        this.store = store;
        this.toastr = toastr;

        this.store.subscribe(this.initiateData.bind(this));
        this.$uibModal = $uibModal;
        this.init();
    }

    initiateData(state) {
        if (state) {
            this.projects = state.projects;
        }
    }

    startTime() {
        let today = new Date();
        let h = today.getHours();
        if (h < 10) {
            h = "0" + h;
        }
        let m = today.getMinutes();
        if (m < 10) {
            m = "0" + m;
        }
        let time = h + ":" + m;
        return time
    }

    startDate(timeStamp) {
        var today = new Date();

        if (timeStamp) {
            today = new Date(Number(timeStamp));
        }
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = dd + "-" + mm + "-" + yyyy;
        return today;
    }

    stampDate(date) {
        if (date) {
            if (date.length == 10) {
                date = date.split('-');

                let newDate = date[1] + "," + date[0] + "," + date[2];

                let today = new Date(newDate).getTime();
                // console.log(today);
                return today;
            } else {
                return date;
            }
        } else {
            let today = new Date().getTime();
            return today
        }
    }

    init() {
      console.log('projects controller has been called');
        this.projectsService.getProjects();
        if (this.$routeParams.id) {
            var projectId = this.$routeParams.id;

            this.projectsService.getProject(projectId).then((project) => {
                this.project = project;
                this.sessionsService.getProjectSessions(projectId).then((sessions) => {
                    sessions.forEach((session) => {
                        session.date = this.startDate(session.date);
                        if (!session.end_time) {
                            session.demo_end_time = this.startTime();
                        }
                    });
                    this.sessions = sessions;
                    this.session = {};
                    this.session.date = this.startDate();
                    this.session.start_time = this.startTime();
                });
                this.notesService.getNote(this.stampDate(this.startDate())).then((note) => {
                    if (note == undefined) {
                        var note = {
                            date: this.stampDate(this.startDate())
                        };
                        this.notesService.addNote(note).then((note) => {
                            this.init();
                        });
                    } else {
                        this.note = note;
                        this.note.date = this.startDate(note.date);
                        note.project_id = this.$routeParams.id;
                    }
                });
            });
            this.projectId = this.$routeParams.id;
        }
    }

    openNoteModal(note) {
        var modalScope = this.$scope.$new();
        modalScope.note = note;
        var instance = this.$uibModal.open({
            templateUrl: '/tracker/template/note.html',
            windowClass: 'note-modal-window',
            scope: modalScope
        });
    }

    updateNote(note) {
        let date = note.date;
        note.date = this.stampDate(note.date);
        this.notesService.updateNote(note);
        note.date = date;
    }

    openProjectModal(project) {
        var modalScope = this.$scope.$new();
        modalScope.project = project;
        // var selecedProject = project;
        var instance = this.$uibModal.open({
            templateUrl: "/tracker/template/project.html",
            scope: modalScope
        });
        if (!project) {
            instance.result.then((project) => {
                if (!project) {
                    this.toastr.error('Project should have a "Name", Enter a name to continue');
                } else {
                    this.projectsService.addProject(project);
                }
            });
        } else {
            instance.result.then((project) => {
                if (!project) {
                    this.toastr.error('Project should have a "Name", Enter a name to continue');
                } else {
                    this.projectsService.updateProject(project);
                }
            });
        }

    }

    openProjectDeleteModal(projectId) {
      console.log('Deleting project nr: ', projectId);

        var instance = this.$uibModal.open({
            templateUrl: "/tracker/template/project-confirmation.html"
        });
        instance.result.then(() => {
            this.projectsService.deleteProject(projectId);
            window.location = '/';
        });
    }



    openSessionDeleteModal(session) {
        var modalScope = this.$scope.$new();
        modalScope.session = session;
        var instance = this.$uibModal.open({
            templateUrl: "/tracker/template/session-confirmation.html",
            scope: modalScope
        });
        instance.result.then((id) => {
            this.sessionsService.deleteSession(id).then(() => {
                this.init();
            });
        });
    }

    addSession(session) {
        if (!session || !session.start_time) {
            this.toastr.error('A Start Time and A Date Should be Provided');
        } else if (this.inputState == 'error') {
            this.toastr.error("Time or Date valid, can't save");
        } else {
            session.date = this.stampDate(session.date);
            session.project_id = this.$routeParams.id;
            this.sessionsService.addSession(session);
            this.init();
        }
    }

    updateSession(session) {
        if (this.inputState == 'error') {
            this.toastr.error("Time or Date valid, can't update");
        } else {
            session.end_time = session.demo_end_time;
            delete session.demo_end_time;
            session.date = this.stampDate(session.date);
            this.sessionsService.updateSession(session).then((res) => {
                console.log(session);
            })
        }
    }


    getTimeAsNum(time) {
        let numTime = time.split(':');
        numTime[0] = Number(numTime[0]);
        numTime[1] = Number(numTime[1]);
        return numTime;
    }

    isValidTime(sessionTime) {
        this.inputState = 'clear';
        let time = this.getTimeAsNum(sessionTime);
        if (time[0] >= 24 || time[1] >= 60) {
            this.toastr.error('Time not valid');
            this.inputState = 'error';
        } else {
            this.inputState = 'clear';
        }
        console.log(this.inputState);

    }

    isValidDate(date) {
        let valid = true;
        date = date.split('-');

        let day = Number(date[0]);
        let month = Number(date[1]);
        let year = Number(date[2]);
        if ((month < 1) || (month > 12)) valid = false;
        else if ((day < 1) || (day > 31)) valid = false;
        else if (((month == 4) || (month == 6) || (month == 9) || (month == 11)) && (day > 30)) valid = false;
        else if ((month == 2) && (((year % 400) == 0) || ((year % 4) == 0)) && ((year % 100) != 0) && (day > 29)) valid = false;
        else if ((month == 2) && ((year % 100) == 0) && (day > 29)) valid = false;
        else if ((month == 2) && (day > 28)) valid = false;
        else if (isNaN(day) && isNaN(month) && isNaN(year)) valid = false;
        if (valid == false) {
            this.toastr.error('Date not valid. please try again!');
            this.inputState = 'error';
        } else {
            this.inputState = 'clear';
        }
        return valid;
    }

    getTimeDifference(startTime, endTime) {
        let start_time = this.getTimeAsNum(startTime);
        let end_time = this.getTimeAsNum(endTime);

        if (end_time[0] <= start_time[0]) {
            end_time[0] = end_time[0] + 24;
        }
        if (end_time[1] < start_time[1]) {
            end_time[1] = end_time[1] + 60;

            if (end_time[0] != 0) {
                end_time[0] = end_time[0] - 1;
            }
        }
        let hoursDif = end_time[0] - start_time[0];

        if ((hoursDif == 24) && (end_time[1] >= start_time[1])) hoursDif = 0;
        if ((hoursDif == 0) && (end_time[1] < start_time[1])) hoursDif = 23;

        let minsDif = end_time[1] - start_time[1];
        let timeD = (hoursDif < 10 ? '0' + hoursDif : hoursDif) + ':' + (minsDif < 10 ? '0' + minsDif : minsDif);
        return timeD;
        // }
    }

}
