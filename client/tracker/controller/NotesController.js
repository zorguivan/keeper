export default class NotesController {
    constructor(notesService, $routeParams, $uibModal, $scope, toastr, store) {
        this.notesService = notesService;
        this.$routeParams = $routeParams;
        this.$uibModal = $uibModal;
        this.$scope = $scope;
        this.toastr = toastr;
        this.store = store;

        this.store.subscribe(this.initiateData.bind(this));
        this.init();
    }

    initiateData(state) {
        if (state) {
            this.notes = state.notes;
        }
    }

    startDate(timeStamp) {
        var today = new Date();

        if (timeStamp) {
            today = new Date(Number(timeStamp));
        }
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = dd + "-" + mm + "-" + yyyy;
        return today;
    }

    init() {
        if (this.$routeParams.id) {
            this.projectId = this.$routeParams.id;
            this.notesService.getNotes(this.projectId).then((notes) => {
                notes.forEach((note) => {
                    note.active = false;
                    if (this.startDate(note.date) == this.startDate()) {
                        note.active = true;
                    }
                    note.date = this.startDate(note.date);
                });
                this.notes = notes;
            });
        } else {
          this.toastr.error('Please Select a Project to see the notes');
        }
    }

    openNoteModal(note) {
        var modalScope = this.$scope.$new();
        modalScope.note = note;
        var instance = this.$uibModal.open({
            templateUrl: '/tracker/template/editNote.html',
            windowClass: 'note-modal-window',
            scope: modalScope
        });
    }

    updateNote(note) {
      delete note.active;
        this.notesService.updateNote(note);
        this.init();
    }

    openOldNoteModal(note) {
      var modalScope = this.$scope.$new();
      modalScope.note = note;
      var instance = this.$uibModal.open({
          templateUrl: '/tracker/template/oldNote.html',
          windowClass: 'note-modal-window',
          scope: modalScope
      });
    }

}
