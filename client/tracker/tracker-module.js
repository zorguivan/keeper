import ProjectsService from './service/ProjectsService';
import SessionsService from './service/SessionsService';
import NotesService from './service/NotesService';

import ProjectsController from './controller/ProjectsController';
import NotesController from './controller/NotesController';

import projectsReducer from './reducers/projects-reducer';

import TrackerConfig from './tracker-config';
import MaskedInputDirective from './directives/MaskedInputDirective';


TrackerConfig['$inject'] = ['$routeProvider'];

ProjectsService['$inject'] = ['$http', 'toastr'];
SessionsService['$inject'] = ['$http', 'toastr'];
NotesService['$inject'] = ['$http', 'toastr'];

ProjectsController['$inject'] = ['ProjectsService', 'SessionsService' , 'NotesService' , '$routeParams', '$uibModal', '$scope', 'toastr', 'Store']
NotesController['$inject'] = ['NotesService', '$routeParams', '$uibModal', '$scope', 'toastr', 'Store']

angular.module('tracker', [])
    .controller('ProjectsController', ProjectsController)
    .controller('NotesController', NotesController)
    .service('ProjectsService', ProjectsService)
    .service('SessionsService', SessionsService)
    .service('NotesService', NotesService)
    .directive('masked', MaskedInputDirective)
    .config(TrackerConfig);

export default 'tracker';
