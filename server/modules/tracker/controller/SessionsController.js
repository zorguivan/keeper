var SessionsProvider = require('../provider/SessionsProvider');

module.exports = SessionsController();

function SessionsController() {
  return {
    addSession: addSession,
    getProjectSessions: getProjectSessions,
    getSessions: getSessions,
    updateSession: updateSession,
    deleteSession: deleteSession
  }

  function addSession(req, res, next){
    SessionsProvider.addSession(req.body).then(function(result){
      console.log('adding session', req.body);
      res.json(result);
    }).catch(function(err){
      res.status(500).send(err);
    });
  }

  function getProjectSessions(req, res, next) {
    SessionsProvider.getProjectSessions(req.params.id).then(function(result){
      res.json(result);
    }).catch(function(error){
      res.status(500).send(error);
      console.log(error);
    });
  }

  function getSessions(req, res, next){
    SessionsProvider.getSessions().then(function(result){
      res.json(result);
    }).catch(function(error){
      console.log(error);
      res.status(500).send(error);
    });
  }

  function updateSession(req, res, next){
    console.log('Update request', res.body);
    SessionsProvider.updateSession(req.body).then(function(result){
      res.json(result);
    }).catch(function(error){
      console.log(error);
      res.status(500).send(error);
    });
  }

  function deleteSession(req, res, next){
    SessionsProvider.deleteSession(req.params.id).then(function(result){
      res.json(result);
    }).catch(function(error){
      console.log(error);
      res.status(500).send(error);
    });
  }
}
