var q = require('q');

var connection;

function SessionsProvider() {
    return {
        setConnection: setConnection,
        addSession: addSession,
        getProjectSessions: getProjectSessions,
        getSessions: getSessions,
        updateSession: updateSession,
        deleteSession: deleteSession
    }

    function setConnection(conn){
      connection = conn;
    }

    function addSession(session){
      var execution = q.defer();
      var query = "INSERT INTO sessions SET :data";
      connection.query(query, {data: session}, function(err, res){
        if(err){
          console.log(err);
          execution.reject(err);
          return;
        }
        execution.resolve(err);
      });
      return execution.promise;
    }

    function getProjectSessions(projectId){
      var execution = q.defer();
      var query = "SELECT * FROM sessions WHERE project_id = :id";
      connection.query(query, {id: projectId}, function(err, res){
        if(err){
          console.log(err);
          execution.reject(err);
          return;
        }
        execution.resolve(res);
      });
      return execution.promise;
    }

    function getSessions(){
      var execution = q.defer();
      var query = "SELECT * FROM sessions";
      connection.query(query, function(err, res){
        if(err){
          console.log(err);
          execution.reject(err);
          return;
        }
        execution.resolve(res);
      });
      return execution.promise;
    }

    function updateSession(session){
      console.log(session);
      var execution = q.defer();
      var query = "UPDATE sessions SET :data where id = :id";
      connection.query(query, {data: session, id: session.id}, function(err, res){
        if(err){
          console.log(err);
          execution.reject(err);
          return;
        }
        execution.resolve(res);
      });
      return execution.promise;
    }
    function deleteSession(id){
      var execution =q.defer();
      var query = "DELETE FROM sessions WHERE id = :id";
      connection.query(query, {id : id}, function(err, res){
        if(err){
          console.log(err);
          execution.reject(err);
        }
        execution.resolve(res);
      });
      return execution.promise;
    }
}

module.exports = SessionsProvider();
